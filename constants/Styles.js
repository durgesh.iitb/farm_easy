export const Shadow = {
    shadowColor:'black',
    shadowOffset:{
        width:0,
        height:2
    },
    shadowRadius:6,
    elevation: 10,
    shadowOpacity:0.26,
    backgroundColor:'white',
}

export const Fonts = {
    font1 : 20,
    font2 : 18,
    font3 : 16,
    font4 : 14,
}

export const errorStyle = {
    color:'#f27f7f',
}
