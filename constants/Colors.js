export default {
    prime : '#262627',
    activeTab: '#929292',
    textPrimeColor: 'rgba(0,0,0,0.7)',
    cancelButtonColor: '#BC312A'
}