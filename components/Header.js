import React from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import Colors from '../constants/Colors'; 

const Header = (props) => {

    const addQueryModalHandler = () => {
        props.onClickAddButton(true)
    }

    return (
        <View style={styles.header}>
            <Text style={styles.logo}>Logo</Text>
            <Button title='+ADD' onPress={addQueryModalHandler} />
        </View>
    )
};

const styles = StyleSheet.create({
    header: {
        width: '100%',
        height: 70,
        paddingTop: 20,
        paddingHorizontal:20,
        backgroundColor: Colors.prime,
        alignItems: 'center',
        flexDirection:'row',
        justifyContent: 'space-between',
    },
    logo: {
        color:'white',
        fontSize:18,
    }
})

export default Header;