import React from 'react';
import { Button, StyleSheet, Text, View, Image, ScrollView } from 'react-native';


const SalesDetails = (props) => {

    return (
        <View style={styles.salesDetailsPage}>
            <View style={styles.topContainer}>
                <Button title='<<- Back' onPress={() => props.setSalesDetailsPage(false)} />
                <Text >Sales Details</Text>
            </View>
            <ScrollView style={styles.scrollView}>
                <View style={styles.sellerContainer}>
                    <View style={styles.dataRow}>
                        <View style={styles.leftColumn}>
                            <Text style={styles.dataLable}>Name</Text>
                            <Text style={styles.dataValue}>{props.salesDetailsData.name}</Text>
                        </View>
                        <View style={styles.rightColumn}>
                            {/* <Text style={styles.dataLable}>Mobile</Text>
                            <Text style={styles.dataValue}>{props.salesDetailsData.mobile}</Text> */}
                            <Text style={styles.dataLable}>Village</Text>
                            <Text style={styles.dataValue}>{props.salesDetailsData.village}</Text>
                        </View>
                    </View>
                    <View style={styles.dataRow}>
                        <View style={styles.leftColumn}>
                            <Text style={styles.dataLable}>Town</Text>
                            <Text style={styles.dataValue}>{props.salesDetailsData.city}</Text>
                        </View>
                        <View style={styles.rightColumn}>
                            <Text style={styles.dataLable}>District</Text>
                            <Text style={styles.dataValue}>{props.salesDetailsData.district}</Text>
                        </View>
                    </View>
                    <View style={styles.dataRow}>
                        <View style={styles.leftColumn}>
                            <Text style={styles.dataLable}>Crop Name</Text>
                            <Text style={styles.dataValue}>{props.salesDetailsData.crop}</Text>
                        </View>
                        <View style={styles.rightColumn}>
                            <Text style={styles.dataLable}>Quantity</Text>
                            <Text style={styles.dataValue}>{props.salesDetailsData.quantity}</Text>
                        </View>
                    </View>
                    {
                        props.salesDetailsData.comment ?
                            <View style={{ ...styles.dataRow, ...styles.commentRow }}>
                                <View>
                                    <Text style={styles.dataLable}>Comment</Text>
                                    <Text style={{ ...styles.dataValue, ...styles.comment }}>{props.salesDetailsData.comment}</Text>
                                </View>
                            </View>
                            : ''
                    }
                    <View style={styles.dataRow}>
                        <View style={styles.imageColumn}>
                            {props.salesDetailsData.images.map((img_url, idx) =>
                                /* <Image source={{uri:Image.resolveAssetSource(TestImage).uri}}/> */
                                <View style={styles.imageView} key={idx}><Image style={styles.cropImage} source={require('../assets/images/test.png')} /></View>
                            )}
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
};

const styles = StyleSheet.create({
    salesDetailsPage: {
        flex: 1
    },
    topContainer: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15
    },
    scrollView: {
        // flexGrow:1
    },
    sellerContainer: {
        paddingHorizontal: 15,
    },
    dataRow: {
        marginVertical: 25,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    leftColumn: {
        width: '40%'
    },
    rightColumn: {
        width: '45%'
    },
    commentRow: {
        marginVertical: 20,
    },
    dataLable: {
        fontSize: 12
    },
    dataValue: {
        paddingVertical: 5,
        fontSize: 18,
        borderBottomColor: 'rgba(0,0,0,0.2)',
        borderBottomWidth: 1,
    },
    comment: {
        borderBottomColor: 'rgba(0,0,0,0.2)',
        borderBottomWidth: 1,
        marginTop: 5,
        fontSize: 18,
        // textAlign:'justfy'
    },
    imageColumn: {
        width: '100%'
    },
    imageView: {
        marginVertical:20
    },
    cropImage: {
        width: '100%',
        height: 200,
    }
})

export default SalesDetails;