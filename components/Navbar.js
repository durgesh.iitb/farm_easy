import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Colors from '../constants/Colors';

const Navbar = (props) => {
    return (
        <View style={styles.navbar}>
            <Text style={props.activeSellTab ? {...styles.headings, ...styles.activeHeading} : styles.headings} onPress={() => props.onClickTab(true)} >TO SELL </Text>
            <Text style={props.activeSellTab ? styles.headings : {...styles.headings, ...styles.activeHeading}} onPress={() => props.onClickTab(false)} >TO BUY</Text>
        </View>
    )
};

const styles = StyleSheet.create({
    navbar: {
        width: '100%',
        height: 50,
        paddingHorizontal: 20,
        backgroundColor: Colors.prime,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    headings: {
        width: '46%',
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        textAlignVertical: 'center'
    },
    activeHeading: {
        backgroundColor: Colors.activeTab,
    }

})

export default Navbar;