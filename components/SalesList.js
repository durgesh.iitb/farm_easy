import React from 'react';
import { StyleSheet, Text, View, FlatList, Button } from 'react-native';
import Card from './Card';


const sellers = [
    { id: 1, 
        name: 'Durgesh', 
        mobile: '9967267027', 
        district: 'Sehore',
        city: 'Mumbai',
        village:'Baisad', 
        crop: 'A', 
        quantity: '20q', 
        comment:'Hi I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027', 
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png'] },
    { id: 2, 
        name: 'Durgesh', 
        mobile: '9967267027', 
        district: 'Sehore',
        city: 'Mumbai',
        village:'Baisad', 
        crop: 'A', 
        quantity: '20q', 
        comment:'Hi I\'m Nanu, I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027', 
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png'] },
    { id: 3, 
        name: 'Durgesh', 
        mobile: '9967267027', 
        district: 'Sehore',
        city: 'Mumbai',
        village:'Baisad', 
        crop: 'A', 
        quantity: '20q', 
        comment:'Hi I\'m Goldy, I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027', 
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png'] },
    { id: 4, 
        name: 'Durgesh', 
        mobile: '9967267027', 
        district: 'Sehore',
        city: 'Mumbai',
        village:'Baisad', 
        crop: 'A', 
        quantity: '20q', 
        comment:'Hi I\'m Duggu, I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027', 
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png'] },
    { id: 5, 
        name: 'Durgesh', 
        mobile: '9967267027', 
        district: 'Sehore',
        city: 'Mumbai',
        village:'Baisad', 
        crop: 'A', 
        quantity: '20q', 
        comment:'Hi I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027', 
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png'] },
    { id: 6, 
        name: 'Durgesh', 
        mobile: '9967267027', 
        district: 'Sehore',
        city: 'Mumbai',
        village:'Baisad', 
        crop: 'A', 
        quantity: '20q', 
        comment:'Hi I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027', 
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png'] },
    { id: 7, 
        name: 'Durgesh', 
        mobile: '9967267027', 
        district: 'Sehore',
        city: 'Mumbai',
        village:'Baisad', 
        crop: 'A', 
        quantity: '20q', 
        comment:'Hi I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027', 
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png'] },
    { id: 8, 
        name: 'Durgesh', 
        mobile: '9967267027', 
        district: 'Sehore',
        city: 'Mumbai',
        village:'Baisad', 
        crop: 'A', 
        quantity: '20q', 
        comment:'Hi I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027', 
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png'] },
    { id: 9, 
        name: 'Durgesh', 
        mobile: '9967267027', 
        district: 'Sehore',
        city: 'Mumbai',
        village:'Baisad', 
        crop: 'A', 
        quantity: '20q', 
        comment:'Hi I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027', 
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png'] },
    { id: 10, 
        name: 'Durgesh', 
        mobile: '9967267027', 
        district: 'Sehore',
        city: 'Mumbai',
        village:'Baisad', 
        crop: 'A', 
        quantity: '20q', 
        comment:'Hi I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027', 
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png'] },
    { id: 11, 
        name: 'Durgesh', 
        mobile: '9967267027', 
        district: 'Sehore',
        city: 'Mumbai',
        village:'Baisad', 
        crop: 'A', 
        quantity: '20q', 
        comment:'Hi I\'m Chhotu, I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027', 
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png'] },
    { id: 12, 
        name: 'Durgesh', 
        mobile: '9967267027', 
        district: 'Sehore',
        city: 'Mumbai',
        village:'Baisad', 
        crop: 'A', 
        quantity: '20q', 
        comment:'Hi I\'m Chhotu, I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027', 
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png'] }
]

const SellersList = (props) => {
    return (
        <View>
            {/* <Text >Sellers</Text> */}
            <FlatList
                keyExtractor={item => item.id.toString()}
                data={sellers}
                renderItem={itemData =>
                    <View style={styles.srollPage}>
                        <Card style={styles.sellerContainer}>
                            <View>
                                <Text style={styles.detailText}>{itemData.item.name}</Text>
                                {/* <Text style={styles.detailText}>{itemData.item.mobile}</Text> */}
                                <Text style={styles.detailText}>{itemData.item.city}</Text>
                            </View>
                            <View>
                                <Text style={styles.detailText}>{itemData.item.crop}</Text>
                                <Text style={styles.detailText}>{itemData.item.quantity}</Text>
                            </View>
                            <View style={styles.detailPageButton}><Button title='Details' onPress={() => {props.setSalesDetailsData(itemData.item); props.setSalesDetailsPage(true)}} /></View>
                        </Card>
                    </View>
                }
            />
        </View>
    )
};

const styles = StyleSheet.create({
    srollPage: {
        padding: 15,
    },
    sellerContainer: {
        flex: 1,
        marginVertical: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    detailText: {
        marginVertical: 2
    },
    detailPageButton: {
        alignSelf:'center'
    }
})

export default SellersList;