import React, { useState } from 'react';
import { Button, StyleSheet, Text, View, ScrollView, Keyboard } from 'react-native';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import { errorStyle } from '../constants/Styles';


const BuyQueryForm = (props) => {
    const [nameInput, setNameInput] = useState('');
    const [villageInput, setVillageInput] = useState('');
    const [tehsilInput, setTehsilInput] = useState('');
    const [districtInput, setDistrictInput] = useState('');
    const [cropInput, setCropInput] = useState('');
    const [quantityInput, setQuantityInput] = useState('');
    const [commentInput, setCommentInput] = useState('');
    const [images, setImages] = useState([]);
    const [emptyInputError, setEmptyInputError] = React.useState(null);

    const onChangeInput = () => {
        setEmptyInputError(null)
    }

    const submitForm = () => {
        Keyboard.dismiss();
        console.log(nameInput)
        console.log(villageInput)
        console.log(tehsilInput)
        console.log(districtInput)
        console.log(cropInput)
        console.log(quantityInput)
        console.log(commentInput)
        if (
            nameInput === "" ||
            villageInput === "" ||
            tehsilInput === "" ||
            districtInput === "" ||
            cropInput === "" ||
            quantityInput === "" ||
            commentInput === ""
        ) {
            setEmptyInputError('Please fill all the fields')
            return;
        }
    }

    return (
        <View style={styles.queryForm}>
            <View style={styles.topContainer}>
                <Button title='<<- Back' onPress={() => props.setShowBuyQueryForm(false)} />
                <Text> Add crop details which you want to purchase </Text>
            </View>
            <ScrollView style={styles.scrollView}>
                <View style={styles.sellerContainer}>
                    <View style={styles.dataRow}>
                        <View style={styles.leftColumn}>
                            {/* <Text style={styles.dataLable}>Name</Text>
                            <TextInput
                                placeholder="Name"
                                style={styles.dataValue}
                                onChangeText={(name)=>setNameInput(name)}
                                value={nameInput}
                            /> */}
                            <FloatingLabelInput
                                label="Name"
                                value={nameInput}
                                onChangeText={name => {setNameInput(name); onChangeInput()}}
                                containerStyles={{
                                    borderBottomWidth: 2,
                                    paddingTop: 20,
                                    paddingBottom: 3,
                                    backgroundColor: '#fff',
                                    borderBottomColor: '#49658c'
                                }}
                            />
                        </View>
                        <View style={styles.rightColumn}>
                            {/* <Text style={styles.dataLable}>Village</Text>
                            <Text style={styles.dataValue}>Baisad</Text> */}
                            <FloatingLabelInput
                                label="Village"
                                value={villageInput}
                                onChangeText={village => {setVillageInput(village); onChangeInput()}}
                                containerStyles={{
                                    borderBottomWidth: 2,
                                    paddingTop: 20,
                                    paddingBottom: 3,
                                    backgroundColor: '#fff',
                                    borderBottomColor: '#49658c'
                                }}
                            />
                        </View>
                    </View>
                    <View style={styles.dataRow}>
                        <View style={styles.leftColumn}>
                            {/* <Text style={styles.dataLable}>Tehsil</Text>
                            <Text style={styles.dataValue}>Nasrullaganj</Text> */}
                            <FloatingLabelInput
                                label="Tehsil"
                                value={tehsilInput}
                                onChangeText={tehsil => {setTehsilInput(tehsil); onChangeInput()}}
                                containerStyles={{
                                    borderBottomWidth: 2,
                                    paddingTop: 20,
                                    paddingBottom: 3,
                                    backgroundColor: '#fff',
                                    borderBottomColor: '#49658c'
                                }}
                            />
                        </View>
                        <View style={styles.rightColumn}>
                            {/* <Text style={styles.dataLable}>District</Text>
                            <Text style={styles.dataValue}>Sehore</Text> */}
                            <FloatingLabelInput
                                label="District"
                                value={districtInput}
                                onChangeText={dist => {setDistrictInput(dist); onChangeInput()}}
                                containerStyles={{
                                    borderBottomWidth: 2,
                                    paddingTop: 20,
                                    paddingBottom: 3,
                                    backgroundColor: '#fff',
                                    borderBottomColor: '#49658c'
                                }}
                            />
                        </View>
                    </View>
                    <View style={styles.dataRow}>
                        <View style={styles.leftColumn}>
                            {/* <Text style={styles.dataLable}>Crop Name</Text>
                            <Text style={styles.dataValue}>wheat</Text> */}
                            <FloatingLabelInput
                                label="Crop Name"
                                value={cropInput}
                                onChangeText={crop => {setCropInput(crop); onChangeInput()}}
                                containerStyles={{
                                    borderBottomWidth: 2,
                                    paddingTop: 20,
                                    paddingBottom: 3,
                                    backgroundColor: '#fff',
                                    borderBottomColor: '#49658c'
                                }}
                            />
                        </View>
                        <View style={styles.rightColumn}>
                            {/* <Text style={styles.dataLable}>Quantity</Text>
                            <Text style={styles.dataValue}>50 Quintal</Text> */}
                            <FloatingLabelInput
                                label="Quantity (in quintal)"
                                value={quantityInput}
                                onChangeText={qnt => {setQuantityInput(qnt); onChangeInput()}}
                                keyboardType="numeric"  
                                hint="50"
                                containerStyles={{
                                    borderBottomWidth: 2,
                                    paddingTop: 20,
                                    paddingBottom: 3,
                                    backgroundColor: '#fff',
                                    borderBottomColor: '#49658c'
                                }}
                            />
                        </View>
                    </View>

                    <View style={{ ...styles.dataRow, ...styles.commentRow }}>
                        <View style={styles.commentColumn}>
                            {/* <Text style={styles.dataLable}>Comment</Text>
                            <Text style={{ ...styles.dataValue, ...styles.comment }}>GGG GGG GG GGGG GG G G GGGG gGgajv ahdask askjhkA AKHAHsiusagidb uKHDIUHIA IUSHDIAIOIAFB USDFGYUSDGF SUIS SDUIHISDB IGSDI</Text> */}
                            <FloatingLabelInput
                                label="Comment"
                                value={commentInput}
                                onChangeText={cmt => {setCommentInput(cmt); onChangeInput()}}
                                // containerStyles={{
                                //     borderWidth: 1,
                                //     // paddingTop: 5,
                                //     // paddingBottom: 3,
                                //     // paddingHorizontal:5,
                                //     // backgroundColor: '#fff',
                                //     borderColor: 'rgba(0,0,0,0.7)'
                                // }}
                                maxLength={200}
                                showCountdown={true}
                                multiline={true}
                            />
                        </View>
                    </View>
                    {
                        emptyInputError ?
                            <View style={styles.dataRow}>
                                <Text style={{ ...styles.errorText, ...errorStyle }}>{emptyInputError}</Text>
                            </View>
                            : null
                    }
                    <View style={styles.dataRow}>
                        <View>
                            {/* {props.salesDetailsData.images.map((img_url, idx) =>
                                <View style={styles.imageView} key={idx}><Image style={styles.cropImage} source={require('../assets/images/test.png')} /></View>
                            )} */}
                            <Button title="Submit" onPress={submitForm} />
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
};

const styles = StyleSheet.create({
    queryForm: {
        flex: 1,
        paddingTop: 20
    },
    topContainer: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15
    },
    scrollView: {
        // flexGrow:1
    },
    sellerContainer: {
        paddingHorizontal: 15,
    },
    dataRow: {
        marginVertical: 25,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    leftColumn: {
        width: '40%'
    },
    rightColumn: {
        width: '45%'
    },
    commentColumn: {
        width:'100%',
        // flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
    },
    commentRow: {
        marginVertical: 20,
    },
    dataLable: {
        fontSize: 12
    },
    dataValue: {
        paddingVertical: 5,
        fontSize: 18,
        borderBottomColor: 'rgba(0,0,0,0.2)',
        borderBottomWidth: 2,
    },
    comment: {
        borderBottomColor: 'rgba(0,0,0,0.2)',
        borderBottomWidth: 2,
        marginTop: 5,
        fontSize: 18,
        // textAlign:'justfy'
    },
    errorText:{
        flex:1,
        textAlign:'center',
    },
    imageColumn: {
        width: '100%'
    },
    imageView: {
        marginVertical: 20
    },
    cropImage: {
        width: '100%',
        height: 200,
    }
})

export default BuyQueryForm;