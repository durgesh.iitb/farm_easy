import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import Header from './Header';
import Navbar from './Navbar';
import SalesList from './SalesList';
import BuyersList from './BuyersList';
import SaleDetails from './SaleDetails';
import AddQueryModal from './AddQueryModal';
import SaleQueryForm from './SaleQueryForm';
import BuyQueryForm from './BuyQueryForm';


const Home = (props) => {
    const [activeSellTab, setActiveSellTab] = useState(true);
    const [saleDetailsPage, setSalesDetailsPage] = useState(false);
    const [salesDetailsData, setSalesDetailsData] = useState({});
    const [showAddQueryModal, setShowAddQueryModal] = useState(false);
    const [showSaleQueryForm, setShowSaleQueryForm] = useState(false);
    const [showBuyQueryForm, setShowBuyQueryForm] = useState(false);
    
    // List page
    let  listPage;
    if (activeSellTab){
        if (saleDetailsPage){
            listPage = <SaleDetails setSalesDetailsPage={setSalesDetailsPage} salesDetailsData={salesDetailsData}/>
        }else{
            listPage = <SalesList setSalesDetailsPage={setSalesDetailsPage} setSalesDetailsData={setSalesDetailsData} />
        }
    }else{
        listPage = <BuyersList />
    }

    // HomePage 
    let homePage=(
        <>
        <AddQueryModal 
            visible={showAddQueryModal} 
            setShowAddQueryModal={setShowAddQueryModal} 
            setShowSaleQueryForm={setShowSaleQueryForm}
            setShowBuyQueryForm={setShowBuyQueryForm}
        />
        <Header onClickAddButton={setShowAddQueryModal} />
        <Navbar activeSellTab={activeSellTab} onClickTab={setActiveSellTab} />
        {listPage}
        </>
    )
    if (showSaleQueryForm){
        homePage = <SaleQueryForm setShowSaleQueryForm={setShowSaleQueryForm} />
    }
    if (showBuyQueryForm){
        homePage = <BuyQueryForm setShowBuyQueryForm={setShowBuyQueryForm} />
    }

    return (
        <View style={styles.home}>
            {homePage}
        </View>
    )
};

const styles = StyleSheet.create({
    home: {
        flex:1
    },
})


export default Home;