import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import Card from './Card';
import Colors from '../constants/Colors';


const buyers = [
    {
        id: 1,
        name: 'Durgesh',
        mobile: '9967267027',
        district: 'Sehore',
        city: 'Nasrullaganj',
        village: 'Baisad',
        crop: 'A',
        quantity: '20q',
        comment: 'Hi I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027',
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png']
    },
    {
        id: 2,
        name: 'Nanu',
        mobile: '9967267027',
        district: 'Sehore',
        city: 'Nasrullaganj',
        village: 'Baisad',
        crop: 'A',
        quantity: '20q',
        comment: 'Hi I\'m Nanu, I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027',
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png']
    },
    {
        id: 3,
        name: 'Goldy',
        mobile: '9967267027',
        district: 'Sehore',
        city: 'Nasrullaganj',
        village: 'Baisad',
        crop: 'A',
        quantity: '20q',
        comment: 'Hi I\'m Goldy, I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027',
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png']
    },
    {
        id: 4,
        name: 'Duggu',
        mobile: '9967267027',
        district: 'Sehore',
        city: 'Nasrullaganj',
        village: 'Baisad',
        crop: 'A',
        quantity: '20q',
        comment: 'Hi I\'m Duggu, I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027',
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png']
    },
    {
        id: 5,
        name: 'Chhotu',
        mobile: '9967267027',
        district: 'Sehore',
        city: 'Nasrullaganj',
        village: 'Baisad',
        crop: 'A',
        quantity: '20q',
        comment: 'Hi I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027',
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png']
    },
    {
        id: 6,
        name: 'Chhotu',
        mobile: '9967267027',
        district: 'Sehore',
        city: 'Nasrullaganj',
        village: 'Baisad',
        crop: 'A',
        quantity: '20q',
        comment: 'Hi I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027',
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png']
    },
    {
        id: 7,
        name: 'Chhotu',
        mobile: '9967267027',
        district: 'Sehore',
        city: 'Nasrullaganj',
        village: 'Baisad',
        crop: 'A',
        quantity: '20q',
        comment: 'Hi I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027',
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png']
    },
    {
        id: 8,
        name: 'Chhotu',
        mobile: '9967267027',
        district: 'Sehore',
        city: 'Nasrullaganj',
        village: 'Baisad',
        crop: 'A',
        quantity: '20q',
        comment: 'Hi I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027',
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png']
    },
    {
        id: 9,
        name: 'Chhotu',
        mobile: '9967267027',
        district: 'Sehore',
        city: 'Nasrullaganj',
        village: 'Baisad',
        crop: 'A',
        quantity: '20q',
        comment: 'Hi I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027',
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png']
    },
    {
        id: 10,
        name: 'Chhotu',
        mobile: '9967267027',
        district: 'Sehore',
        city: 'Nasrullaganj',
        village: 'Baisad',
        crop: 'A',
        quantity: '20q',
        comment: 'Hi I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027',
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png']
    },
    {
        id: 11,
        name: 'Chhotu',
        mobile: '9967267027',
        district: 'Sehore',
        city: 'Nasrullaganj',
        village: 'Baisad',
        crop: 'A',
        quantity: '20q',
        comment: 'Hi I\'m Chhotu, I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027',
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png']
    },
    {
        id: 12,
        name: 'Chhotu',
        mobile: '9967267027',
        district: 'Sehore',
        city: 'Nasrullaganj',
        village: 'Baisad',
        crop: 'A',
        quantity: '20q',
        comment: 'Hi I\'m Chhotu, I want to sell my 20 quintal of wheat with great quality, you can call me on my number - 9967267027',
        images: ['../assets/images/test.png', '../assets/images/test.png', '../assets/images/test.png']
    }
]

const BuyersList = (props) => {
    return (
        <View>
            <FlatList
                keyExtractor={item => item.id.toString()}
                data={buyers}
                renderItem={itemData =>
                    <View style={styles.srollPage}>
                        <Card style={styles.sellerContainer}>
                            <View style={styles.topSection}>
                                <View>
                                    <Text style={styles.detailText}>{itemData.item.name}</Text>
                                    {/* <Text style={styles.detailText}>{itemData.item.mobile}</Text> */}
                                    <Text style={styles.detailText}>{itemData.item.city}</Text>
                                </View>
                                <View>
                                    <Text style={styles.detailText}>{itemData.item.crop}</Text>
                                    <Text style={styles.detailText}>{itemData.item.quantity}</Text>
                                </View>
                            </View>
                            <View style={styles.commentSection}>
                                <Text style={styles.detailText}>{itemData.item.comment}</Text>
                            </View>
                        </Card>
                    </View>
                }
            />
        </View>
    )
};

const styles = StyleSheet.create({
    srollPage: {
        padding: 15,
    },
    sellerContainer: {
        flex: 1,
        marginVertical: 5,
    },
    topSection: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: 15,
        borderBottomWidth: 0.5,
        borderBottomColor: Colors.textPrimeColor
    },
    commentSection: {
        paddingTop:15
    },
    detailText: {
        marginVertical: 2
    },
    detailPageButton: {
        alignSelf: 'center'
    }
})

export default BuyersList;