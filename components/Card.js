import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Shadow } from '../constants/Styles';


const Card = (props) => {
    return (
        <View style={{...styles.card, ...Shadow, ...props.style}}>{props.children}</View>
    )
};


const styles = StyleSheet.create({
    card: {
        padding:20,
        borderRadius:10, 
        
    },
})

export default Card;