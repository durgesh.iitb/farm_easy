import React from 'react';
import { StyleSheet, Text, View, Modal, TouchableOpacity } from 'react-native';
import Colors from '../constants/Colors';
import { Shadow, Fonts } from '../constants/Styles';


const AddQueryModal = (props) => {

    const toSellClickHandler = () => {
        props.setShowAddQueryModal(false)
        props.setShowSaleQueryForm(true)
    }
    
    const toBuyClickHandler = () => {
        props.setShowAddQueryModal(false)
        props.setShowBuyQueryForm(true)
    }

    return (
        <Modal visible={props.visible} animationType="slide">
            <View style={styles.queryModalContainer}>
                <View style={styles.addButtons}>
                    <TouchableOpacity style={{...styles.button, ...Shadow}} onPress={toSellClickHandler}>
                        <Text style={styles.buttonText}>TO SELL</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{...styles.button, ...Shadow}} onPress={toBuyClickHandler}>
                        <Text style={styles.buttonText}>TO BUY</Text>
                    </TouchableOpacity>
                </View>
                <View style={Shadow}>
                    <TouchableOpacity style={{...Shadow, ...styles.cancelButton}} onPress={() => props.setShowAddQueryModal(false)}>
                        <Text style={{...styles.buttonText, ...styles.cancelButtonText}}>CANCEL</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    )
};

const styles = StyleSheet.create({
    queryModalContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    addButtons: {
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },

    button: {
        height:70,
        width:150,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'white',
        marginHorizontal:5,
        borderRadius:5
    },
    buttonText:{
        color:Colors.textPrimeColor,
        fontSize:Fonts.font1,
        fontWeight:'bold'
    },
    cancelButton:{
        backgroundColor:Colors.cancelButtonColor,
        alignItems:'center',
        justifyContent:'center',
        height:30,
        width:100,
        borderRadius:5
    }, 
    cancelButtonText:{
        fontSize:Fonts.font3,
        color:Colors.textPrimeColor,
    }
});

export default AddQueryModal;